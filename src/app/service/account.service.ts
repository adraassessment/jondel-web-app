import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BaseService } from "../shared/base.service";
import { ConfigService } from '../shared/config.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends BaseService {

  constructor( private http: HttpClient) {
    super();    
   }

  uploadAccounts(fileToUpload:any){
    debugger;
    const endpoint = 'https://localhost:44362/api/account/upload';
    let formData: FormData = new FormData();
    formData.append('File', fileToUpload);

      // let headers = new Headers();
      //   /** In Angular 5, including the header Content-Type can invalidate your request */
      //   headers.append('Content-Type', 'multipart/form-data');
      //   headers.append('Accept', 'application/json');

    return  this.http.post(endpoint,formData, { headers:  {observe: 'body'} })
    .pipe(catchError(this.handleError));

  }

  getReportData(startDate:any,endData:any){
    const endpoint = 'https://localhost:44362/api/account/AccountReport';
    const body = {'StartDate':startDate.toISOString(),'EndDate' :endData.toISOString()};

    return  this.http.post<any>(endpoint,body )
    .pipe(catchError(this.handleError));

  }

  getAccountBalance(selectedDate:any){
    debugger
    const endpoint = 'https://localhost:44362/api/account/AccountBalanceDetail';
    const body = {'SelectedDate':selectedDate.toISOString()};
    return  this.http.post<any>(endpoint,body)
    .pipe(catchError(this.handleError));
  }

  


}
