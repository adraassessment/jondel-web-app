import { Component, OnInit } from '@angular/core';
import { AccountBalance } from "../models/account-balance";
import { AccountService } from "../service/account.service";
import { finalize } from 'rxjs/operators';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";

@Component({
  selector: 'app-balance-check',
  templateUrl: './balance-check.component.html',
  styleUrls: ['./balance-check.component.scss']
})
export class BalanceCheckComponent implements OnInit {
  today: any
  sixMonthsAgo: any

  date = new FormControl(moment());


  balances: any;
  constructor(private accountService: AccountService) {

    this.today = new Date();
    this.sixMonthsAgo = new Date();
    this.sixMonthsAgo.setMonth(this.today.getMonth() - 100);
  }
  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
debugger
    this.accountService.getAccountBalance(this.date.value).pipe(finalize(() => {

    })).subscribe(result => {
      debugger
      this.balances = result;
    });

    datepicker.close();
  }


  ngOnInit(): void {
  }

}
