import { Component, OnInit } from '@angular/core';
import { AccountService } from "../service/account.service";
import { finalize } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-balance-report',
  templateUrl: './balance-report.component.html',
  styleUrls: ['./balance-report.component.scss']
})
export class BalanceReportComponent implements OnInit {
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });
  
  constructor(private accountService: AccountService) {

  }

  ngOnInit(): void {

  }

  generate() {

    let startDate = this.range.value.start;
    let endDate = this.range.value.end;
    this.accountService.getReportData(startDate, endDate).pipe(finalize(() => {
      // this.spinner.hide();
      debugger
      // this.busy = false;
    })).subscribe(result => {
      debugger
      this.data = [{ "name": "green", "series": result }];
    });;
  }

  name = 'Angular';

  colorScheme = {
    domain: ['#08DDC1', '#FFDC1B', '#FF5AAA']
  };

  data: any


}
