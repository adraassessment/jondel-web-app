import { Component, OnInit } from '@angular/core';
import { AccountService } from "../service/account.service";
import { finalize } from 'rxjs/operators';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormControl, Validators } from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'app-upload-balance',
  templateUrl: './upload-balance.component.html',
  styleUrls: ['./upload-balance.component.scss']
})

export class UploadBalanceComponent implements OnInit {
  fileToUpload: File = null;
  statusMessage: any

  constructor(private accountService: AccountService) {
  }

  ngOnInit(): void {
  }

  handleFileInput(files: Event) {
    this.statusMessage = "";
    const target = files.target as HTMLInputElement;
    const file: File = (target.files as FileList)[0];
    const fileNameValidator = new RegExp("^\\d{4}-(0[1-9]|1[0-2]).txt$", "g");
    // inf invalid format stop proceed
    if (!fileNameValidator.test(file.name)) {
      this.statusMessage = "not valid format";
      return;
    }

    this.accountService.uploadAccounts(file).pipe(finalize(() => {
    
    })).subscribe(result => {
      this.statusMessage ="upload finished";
     });;
  }




}
