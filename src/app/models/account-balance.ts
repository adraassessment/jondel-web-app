export class AccountBalance {

    constructor(
        public accountName: string,
        public accountBalance: number
      ) {  }
}
