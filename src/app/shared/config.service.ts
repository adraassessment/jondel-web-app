import { Injectable } from '@angular/core';
 
@Injectable()
export class ConfigService {    

    constructor() {}

    // get authApiURI() {
    //     return 'http://localhost:6062/authentication/api';
    // }    
     
    get resourceApiURI() {
        return 'https://localhost:44362/api';
    }  
}