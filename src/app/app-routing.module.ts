import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadBalanceComponent } from "./upload-balance/upload-balance.component";
import { BalanceCheckComponent } from "./balance-check/balance-check.component";
import { BalanceReportComponent } from "./balance-report/balance-report.component";
import { HomeComponent } from "./home/home.component";


const routes: Routes = [
  { path: '', component: HomeComponent },

  { path: 'upload', component: UploadBalanceComponent },
  { path: 'balance-check', component: BalanceCheckComponent },
  { path: 'balance-report', component: BalanceReportComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
